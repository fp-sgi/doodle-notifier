# doodle-notifier

Checks an IMAP INBOX for unread update notifications from Doodle.com and posts 
participants to Mattermost.

## Usage

* `cp .env-dist .env`
* add connection details to `.env`
* `bundle && ruby bin/check.rb`

In production you will probably install a cronjob and pass the connection details using environment variables.
