#!/usr/bin/env ruby
require 'bundler'
Bundler.require
require 'net/http'
require 'uri'
require 'json'

Dotenv.load

$webhook_url = ENV['MATTERMOST_WEBHOOK']

imap_opts = {
  address:    ENV['MAIL_HOST'],
  port:      (ENV['MAIL_PORT'] ? ENV['MAIL_PORT'].to_i : 993),
  user_name:  ENV['MAIL_USER'],
  password:   ENV['MAIL_PASS'],
  enable_ssl: ENV['MAIL_SSL'] == 'true'
}
Mail.defaults { retriever_method :imap, imap_opts }

$subj_regex = %r{\ADoodle.+Update\Z}
$name_regex = %r{"(?<name>.+)"  hat soeben an der Umfrage}

def fetch_names
  Mail.find(what: :first, from: 'Doodle', keys: ['NOT','SEEN']).map { |mail|
    next unless mail.subject.match $subj_regex
    next unless extracted_data = mail.body.decoded.match($name_regex)
    extracted_data['name']
  }.compact
end

def post_notification(name)
  message = "Neuer Doodle-Eintrag von #{name}"
  payload = { username: 'Doodle', text: message }

  uri                  = URI.parse $webhook_url
  request              = Net::HTTP::Post.new(uri)
  request.content_type = 'application/json'
  request.body         = "#{payload.to_json}"

  response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
    http.request(request)
  end

  $STDERR.puts "HTTP status #{response.code}: #{response.body}" unless response.code.to_i == 200
end

fetch_names.each{ |name| post_notification(name) }
