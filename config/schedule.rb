job_type :ruby_script, 'cd :path && ~/.rvm/bin/rvm default do bundle exec :task :output'

set :output, '/var/www/doodle-notifier/shared/cron.log'

env :PATH,     ENV['PATH']
env :GEM_HOME, ENV['GEM_HOME']
env :GEM_PATH, ENV['GEM_HOME']

every 5.minutes do
  ruby_script "bin/check.rb"
end
